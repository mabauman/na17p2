# Note de clarification
## Analyse du sujet

### Terminal
* Propriété d'un client unique, enregistré sur la plateforme
* Décrit par son modèle
* Décrit par un numéro de série unique

### Modèle
* Décrit par un constructeur
* Décrit par une désignation commerciale
* Doté d'un système d'exploitation

### Système d'exploitation
* Identifié par son constructeur et sa version
* Disponible que pour certains modèles de téléphone

### Achat
* Un utilisateur peut acheter pour lui ou un autre client
* Les paiements peuvent être réalisés par carte bancaire ou carte prépayée, et ces informations sont stockées

##### Achat simple (hérite d'achat)
* Permet d'obtenir une application ou une ressource pour une durée illimitée sur tous les terminaux
* L'application ou la ressource a un coût fixe

##### Abonnement (hérite d'achat)
* Permet l'utilisation d'une application (et non une ressource) pour une période déterminée sur tous les terminaux
* L'application peut être téléchargée et installée gratuitement, mais son utilisation nécessite un coût périodique (en mois, trimestres ou années)
* Lors de la souscription, l'utilisateur peut choisir entre une formule automatique ou un nombre de mois
* L'application ou la ressource est installable et accessible pour tous les terminaux enregistrés par ce client, limité à 5

### Carte prépayée
* Possède un montant de départ
* Possède un montant courant <= au montant de départ
* Possède une date de validité

### Editeur
* Décrit par son nom, contact (forme au choix) et url

### Produit
* Proposé par un éditeur
* Un éditeur peut proposer une ressource pour une application dont il n'est pas l'éditeur
* Identifié par un titre
* Disponible que pour certaines versions de certains systèmes d'exploitation

##### Application (hérite de produit)


##### Ressource (hérite de produit)
* Ne s'applique qu'à une seule application

### Avis
* Un utilisateur peut donner un avis sur une application qu'il possède
* Un avis possède une note (sur 5) ainsi qu'un commentaire limité à 500 caractères




## Objets

### Terminal
* Numéro de série unique
* Client enregistré sur la plateforme
* Modèle

### Client
* Pseudo unique

### Modèle
* Constructeur
* Désignation commerciale (son nom)
* Système d'exploitation

### Constructeur
* Nom unique

### Système d'exploitation
* Constructeur
* Version
* Disponible pour certains téléphones

### Achat
* Receveur (qui reçoit le produit)
* Acheteur
* Produit

##### Achat simple (hérite d'achat)

##### Abonnement (hérite d'achat)
* Date d'achat
* Durée
* Renouvellement Auto

### Produit
* Coût
* Proposé par un éditeur
* Titre (unique)
* Disponible pour certains systèmes d'exploitation

##### Application (hérite de produit)

##### Ressource (hérite de produit)
* Application associée

### Carte bancaire
* Utilisateur
* Numéro de carte

### Carte prépayée
* Utilisateur
* Montant de départ
* Montant courant
* Date de validité

### Installation
* Utilisateur
* Application ou ressource
* Terminal

### Editeur
* Nom
* Contact
* url

### Avis
* Utilisateur
* Application
* Note (sur 5)
* Commentaire (500 caractères max)

## Vues

### Utilisateur
* Peut connaitre les applications compatibles avec ses appareils
* Peut avoir un historique de ses achats et de ses installations
* Peut gérer ses terminaux (ajout/retrait)

### Administrateur
* Peut ajouter des applications et des ressources
* Peut ajouter des editeurs
* Peut accorder des cartes prépayées à des clients

### Analyste
* Peut connaître les applications les plus rentables
* Peut connaître les éditeurs qui réalisent le plus de profit et le plus de vente
* Peut connaître le nombre d'installations d'une application/ressources
* Evaluer les porfits réalises par le service distributeur (30% du prix affiché)
* Evaluer les profits réalisés par le service éditeur (70% du prix affiché)
* Connaître les utilisateurs les plus actifs (nombres d'avis)

## Contraintes




## Clarification
* On considère qu'une fois une application téléchargée, elle s'installe automatiquement. On fait donc le choix de ne pas séparer les téléchargements des installations
* On considère qu'une fois qu'un utilisateur a téléchargé et installé une application, il ne la désinstalle pas, et il ne tente pas de la re-télécharger avec le même terminal
* Les avis ne concernent que les applications, pas les ressources
* Le numéro de série d'un terminal est une chaîne de caractères de taille 10.
* Un éditeur peut proposer une ressource pour une application dont il n’est pas l’éditeur
* On considère que les utilisateurs, les constructeurs et les éditeurs possèdent des noms uniques
* On considère que l'on peut acheter des ressources pour des applications que nous ne possédons pas
* On considère que l'on peut recevoir plusieurs fois des applications/ressources
* On considère qu'un utilisateur peut ne pas posséder de terminal
* On considère qu'un système d'exploitation peut ne tourner sur aucun modèle enregistré
* On considère qu'un constructeur peut ne fabriquer aucun des modèles ou des systèmes d'exploitations enregistrés
* On considère qu'une ressource peut être disponible sur un système d'exploitation où l'application n'est pas disponible