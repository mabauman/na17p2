--Ce document répertorie 6 SELECT (dont 2 qui sont les versions "ressources" des SELECT "applications")


--Applications compatible pour un user (ici, "Valentin")
--Matthieu et Valentin peuvent installer 4 applications, et Jean une seule
SELECT application.titre
FROM application
INNER JOIN ApplicationDisponibleSur
ON application.titre = ApplicationDisponibleSur.application
INNER JOIN Modele
ON Modele.systeme = ApplicationDisponibleSur.systeme
INNER JOIN Terminal
ON Terminal.modele = Modele.designation
WHERE Terminal.proprietaire='Valentin';

--Ressources compatible pour un user (ici, "Valentin")
--Matthieu et Valentin peuvent installer 3 ressources, et Jean une seule
SELECT Ressource.titre
FROM Ressource
INNER JOIN RessourceDisponibleSur
ON Ressource.titre = RessourceDisponibleSur.ressource
INNER JOIN Modele
ON Modele.systeme = RessourceDisponibleSur.systeme
INNER JOIN Terminal
ON Terminal.modele = Modele.designation
WHERE Terminal.proprietaire='Valentin';

--Historique des achats d'applications (nom et prix) pour un user (ici, "Matthieu")
--Matthieu possède 3 achats d'applications,Jean 2, et Valentin 1
SELECT Application.titre,Application.cout
FROM Application
INNER JOIN AchatApplication
ON AchatApplication.application = Application.titre
WHERE achatapplication.acheteur = 'Matthieu';

--Historique de ses achats de ressources (nom et prix)
--Matthieu possède 2 achats de ressources,Jean 1, et Valentin 0
SELECT Ressource.titre, Ressource.cout
FROM Ressource
INNER JOIN AchatRessource
ON AchatRessource.ressource = Ressource.titre
WHERE achatRessource.acheteur = 'Matthieu';

--Nombre d'installations d'une application
--Calculatrice et WindowsSmartphone : 2, Minecraft et SmashBros : 1
SELECT InstallationApplication.application,COUNT(InstallationApplication.application) as nbInstallation
FROM InstallationApplication
GROUP BY InstallationApplication.application
ORDER BY nbInstallation DESC;


--Connaître les utilisateurs les plus actifs
--Matthieu possède 3 avis, Jean 2, Valentin 1
SELECT Avis.utilisateur,COUNT(Avis.utilisateur) as nbAvis
FROM Avis
GROUP BY Avis.utilisateur
ORDER BY nbAvis DESC;

--Connaître le nombre d'application d'un editeur
--MicrosoftFrance : 2, Mojang et Nintendo : 1
SELECT Editeur.nom,COUNT(Application.titre) as nb
FROM Editeur
INNER JOIN Application
ON Editeur.nom = Application.editeur
GROUP BY Editeur.nom
ORDER BY nb DESC

--Connaître le nombre d'applications installées par un utilisateur (en comptant tous ses terminaux, et si un utilisateur a installé une application sur 2 de ses terminaux, cela compte pour 2 installations distinctes)
--Matthieu : 4, Jean et Valentin : 1
SELECT Utilisateur.pseudo, Count(InstallationApplication.application) as nb
FROM Utilisateur
INNER JOIN Terminal
ON Terminal.proprietaire = Utilisateur.pseudo
INNER JOIN InstallationApplication
ON InstallationApplication.terminal = Terminal.num
GROUP BY Utilisateur.pseudo
ORDER BY nb DESC;
