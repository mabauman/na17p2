--Concernant les transactions :
--On va commencer avec une première transaction, contenant toutes mes requêtes SAUF les derniers inserts, que l'on va commit.
--On va enchainer avec une nouvelle transaction, contenant les inserts de RessourceDisponibleSur, que l'on va rollback.
--On va terminer avec une dernière transaction, contenant les mêmes inserts, mais qui seront cette fois ci commit. 
--Le fait que ce code fonctionne montre que la 2ème transaction a bien été rollback, sinon la 3ème transaction aurait rencontré des problèmes d'insertion.


CREATE SCHEMA Nimpstore;
SET search_path TO Nimpstore;

BEGIN TRANSACTION;
--Utilisateur(#pseudo: String)
CREATE TABLE Utilisateur (
	pseudo varchar primary key
);

--CarteBancaire(#num: String,proprietaire => Utilisateur.pseudo) avec taille(num)=12
CREATE TABLE CarteBancaire (
	num varchar primary key,
	proprietaire varchar,
	foreign key (proprietaire) references Utilisateur(pseudo),
	check(LENGTH(num)=12)
);

--CartePrepayee(#id: int, montantDepart: float, montantCourant: float, dateValidite: Date, proprietaire => Utilisateur.pseudo) avec montantDepart et montantCourant > 0, et montantCourant < montantDepart
CREATE TABLE CartePrepayee (
	id int primary key,
	montantDepart float not null,
	montantCourant float not null,
	dateValidite date not null,
	proprietaire varchar,
	foreign key (proprietaire) references Utilisateur(pseudo),
	check((montantDepart >= 0) AND (montantCourant >= 0) AND (montantCourant <= montantDepart))
);

--Constructeur(#nom: String)
CREATE TABLE Constructeur (
	nom varchar primary key
);

--SystemeExploitation(#id: int, constructeur=>Constructeur.nom, version: String)
CREATE TABLE SystemeExploitation (
	id int primary key,
	constructeur varchar,
	version varchar not null,
	foreign key (constructeur) references Constructeur(nom)
);

--Modele(#designation: String, constructeur=>Constructeur.nom, systeme=>SystemeExploitation.id)
CREATE TABLE Modele (
	designation varchar primary key,
	constructeur varchar,
	systeme int,
	foreign key (constructeur) references Constructeur(nom),
	foreign key (systeme) references SystemeExploitation(id)
);

--Terminal(#num: String, modèle=>Modele.designation,proprietaire=>Utilisateur.pseudo) avec Taille(num) = 10)
CREATE TABLE Terminal (
	num varchar primary key,
	modele varchar,
	proprietaire varchar,
	foreign key (modele) references Modele(designation),
	foreign key (proprietaire) references Utilisateur(pseudo),
	check(LENGTH(num) = 10)
);



--Editeur(#nom: String, contact: String, url: String)
CREATE TABLE Editeur (
	nom varchar primary key,
	contact varchar not null,
	url varchar not null
);

--Application(#titre: String, cout: float, editeur=>Editeur.nom) avec cout >= 0
CREATE TABLE Application (
	titre varchar primary key,
	cout float check(cout >= 0) not null,
	editeur varchar,
	abonnement boolean not null,
	foreign key (editeur) references Editeur(nom)
);

--Ressource(#titre: String, cout: float, application=>Application.titre, editeur=>Editeur.nom) avec cout >= 0
CREATE TABLE Ressource (
	titre varchar primary key,
	cout float check(cout >= 0) not null,
	application varchar,
	editeur varchar,
	foreign key (application) references Application(titre),
	foreign key (editeur) references Editeur(nom)
);



--InstallationApplication(#id: int, utilisateur=>Utilisateur.pseudo, terminal=>Terminal.num, application=>Application.titre)
CREATE TABLE InstallationApplication (
	id int primary key,
	terminal varchar,
	application varchar,
	foreign key (terminal) references Terminal(num),
	foreign key (application) references Application(titre)
);

--InstallationRessource(#id: int, utilisateur=>Utilisateur.pseudo, terminal=>Terminal.num, ressource=>Ressource.titre)
CREATE TABLE InstallationRessource (
	id int primary key,
	terminal varchar,
	ressource varchar,
	foreign key (terminal) references Terminal(num),
	foreign key (ressource) references Ressource(titre)
);


--Avis(#utilisateur=>Utilisateur.pseudo, #application=>Application.titre, note: int, commentaire: String) avec (note entre 0 et 5) et (Taille(commentaire)<=500)
CREATE TABLE Avis (
	utilisateur varchar,
	application varchar,
	note int check(note>=0 AND note<=5) not null,
	commentaire varchar check(LENGTH(commentaire) <=500) not null,
	foreign key (utilisateur) references Utilisateur(pseudo),
	foreign key (application) references Application(titre),
	primary key (utilisateur,application)
);

--AchatRessource(#id: int, receveur=>Utilisateur.pseudo, acheteur=>Utilisateur.pseudo, ressource=>Ressource.titre, dateAchat: Date)
CREATE TABLE AchatRessource (
	id int primary key,
	receveur varchar,
	acheteur varchar,
	ressource varchar,
	dateAchat date not null,
	foreign key (receveur) references Utilisateur(pseudo),
	foreign key (acheteur) references Utilisateur(pseudo),
	foreign key (ressource) references Ressource(titre)
);

--AchatApplication(#id: int, receveur=>Utilisateur.pseudo, acheteur=>Utilisateur.pseudo, application=>Application.titre, dateAchat: Date, dateRenouvellement: Date) avec C1
CREATE TABLE AchatApplication (
	id int primary key,
	receveur varchar,
	acheteur varchar,
	application varchar,
	dateAchat date not null,
	dateRenouvellement date,
	foreign key (receveur) references Utilisateur(pseudo),
	foreign key (acheteur) references Utilisateur(pseudo),
	foreign key (application) references Application(titre),
	CHECK (dateRenouvellement > dateAchat)
);


--ApplicationDisponibleSur(#application=>Application.titre, #systeme=>SystemeExploitation.id)
CREATE TABLE ApplicationDisponibleSur (
	application varchar,
	systeme int,
	foreign key (application) references Application(titre),
	foreign key (systeme) references SystemeExploitation(id),
	primary key (application,systeme)
);

--RessourceDisponibleSur(#ressource=>Ressource.titre, #systeme=>SystemeExploitation.id)
CREATE TABLE RessourceDisponibleSur (
	ressource varchar,
	systeme int,
	foreign key (ressource) references Ressource(titre),
	foreign key (systeme) references SystemeExploitation(id),
	primary key (ressource,systeme)
);

INSERT INTO Utilisateur VALUES ('Matthieu');
INSERT INTO Utilisateur VALUES ('Jean');
INSERT INTO Utilisateur VALUES ('Valentin');

INSERT INTO CarteBancaire VALUES ('111122223333','Matthieu');
INSERT INTO CarteBancaire VALUES ('222233334444','Matthieu');
INSERT INTO CarteBancaire VALUES ('333344445555','Matthieu');
INSERT INTO CarteBancaire VALUES ('444455556666','Matthieu');

INSERT INTO CartePrepayee VALUES (1,50,40,'2020-10-24','Matthieu');
INSERT INTO CartePrepayee VALUES (2,200,200,'2020-12-31','Matthieu');
INSERT INTO CartePrepayee VALUES (3,400,300,'2020-5-3','Jean');
INSERT INTO CartePrepayee VALUES (4,500,0,'2020-2-18','Valentin');

INSERT INTO Constructeur VALUES ('Apple');
INSERT INTO Constructeur VALUES ('Samsung');
INSERT INTO Constructeur VALUES ('Xiaomi');

INSERT INTO SystemeExploitation VALUES (1,'Apple','1.0');
INSERT INTO SystemeExploitation VALUES (2,'Apple','1.1');
INSERT INTO SystemeExploitation VALUES (3,'Samsung','2.0');
INSERT INTO SystemeExploitation VALUES (4,'Xiaomi','0.5.2b');

INSERT INTO Modele VALUES ('Iphone 10','Apple',2);
INSERT INTO Modele VALUES ('Iphone 3','Apple',1);
INSERT INTO Modele VALUES ('Galaxy S10','Samsung',3);
INSERT INTO Modele VALUES ('MI A1','Xiaomi',4);

INSERT INTO Terminal VALUES ('0123456789','MI A1','Matthieu');
INSERT INTO Terminal VALUES ('1247354290','Iphone 10','Jean');
INSERT INTO Terminal VALUES ('7304519821','Galaxy S10','Valentin');

INSERT INTO Editeur VALUES ('Mojang','contact@mojang.com','mojang.com');
INSERT INTO Editeur VALUES ('MicrosoftFrance','contact@Microsoft.com','Microsoft.com');
INSERT INTO Editeur VALUES ('MicrosoftBelgique','contact@Microsoft.com','Microsoft.com');
INSERT INTO Editeur VALUES ('Nintendo','contact@Nintendo.com','Nintendo.com');

INSERT INTO Application VALUES ('Minecraft',20,'Mojang',FALSE);
INSERT INTO Application VALUES ('SmashBros',60,'Nintendo',FALSE);
INSERT INTO Application VALUES ('WindowsSmartphone',80,'MicrosoftFrance',TRUE);
INSERT INTO Application VALUES ('Calculatrice',0,'MicrosoftFrance',FALSE);

INSERT INTO Ressource VALUES ('Personnage1',5.99,'SmashBros','Nintendo');
INSERT INTO Ressource VALUES ('PersonnageDeMinecraft',5.99,'SmashBros','Mojang');
INSERT INTO Ressource VALUES ('Multiplication',199.99,'Calculatrice','MicrosoftFrance');

INSERT INTO InstallationApplication VALUES (1,'0123456789','SmashBros');
INSERT INTO InstallationApplication VALUES (2,'0123456789','Calculatrice');
INSERT INTO InstallationApplication VALUES (3,'0123456789','WindowsSmartphone');
INSERT INTO InstallationApplication VALUES (4,'0123456789','WindowsSmartphone');
INSERT INTO InstallationApplication VALUES (5,'1247354290','Minecraft');
INSERT INTO InstallationApplication VALUES (6,'7304519821','Calculatrice');

INSERT INTO InstallationRessource VALUES (1,'0123456789','Personnage1');
INSERT INTO InstallationRessource VALUES (2,'0123456789','Multiplication');
INSERT INTO InstallationRessource VALUES (3,'1247354290','Multiplication');

INSERT INTO Avis VALUES ('Matthieu','SmashBros',5,'Une bonne application');
INSERT INTO Avis VALUES ('Matthieu','Calculatrice',3,'Utile');
INSERT INTO Avis VALUES ('Matthieu','WindowsSmartphone',2,'Bizarre');
INSERT INTO Avis VALUES ('Jean','WindowsSmartphone',2,'Pas tres interressant');
INSERT INTO Avis VALUES ('Jean','Minecraft',4,'Amusant');
INSERT INTO Avis VALUES ('Valentin','Minecraft',4,'Tres bonne experience');

INSERT INTO AchatRessource VALUES (1,'Matthieu','Matthieu','Personnage1','2020-05-20');
INSERT INTO AchatRessource VALUES (2,'Matthieu','Matthieu','Multiplication','2020-05-19');
INSERT INTO AchatRessource VALUES (3,'Jean','Jean','Multiplication','2020-05-18');


INSERT INTO AchatApplication VALUES (1,'Matthieu','Matthieu','SmashBros','2020-05-18',NULL);
INSERT INTO AchatApplication VALUES (2,'Matthieu','Matthieu','Calculatrice','2020-04-12',NULL);
INSERT INTO AchatApplication VALUES (3,'Matthieu','Matthieu','WindowsSmartphone','2020-03-01','2020-06-01');
INSERT INTO AchatApplication VALUES (4,'Jean','Jean','WindowsSmartphone','2020-02-18','2020-06-18');
INSERT INTO AchatApplication VALUES (5,'Jean','Jean','Minecraft','2019-10-24',NULL);
INSERT INTO AchatApplication VALUES (6,'Valentin','Valentin','Calculatrice','2020-02-15',NULL);

INSERT INTO ApplicationDisponibleSur VALUES ('SmashBros',1);
INSERT INTO ApplicationDisponibleSur VALUES ('SmashBros',3);
INSERT INTO ApplicationDisponibleSur VALUES ('SmashBros',4);
INSERT INTO ApplicationDisponibleSur VALUES ('Calculatrice',1);
INSERT INTO ApplicationDisponibleSur VALUES ('Calculatrice',2);
INSERT INTO ApplicationDisponibleSur VALUES ('Calculatrice',3);
INSERT INTO ApplicationDisponibleSur VALUES ('Calculatrice',4);
INSERT INTO ApplicationDisponibleSur VALUES ('WindowsSmartphone',1);
INSERT INTO ApplicationDisponibleSur VALUES ('WindowsSmartphone',3);
INSERT INTO ApplicationDisponibleSur VALUES ('WindowsSmartphone',4);
INSERT INTO ApplicationDisponibleSur VALUES ('Minecraft',1);
INSERT INTO ApplicationDisponibleSur VALUES ('Minecraft',3);
INSERT INTO ApplicationDisponibleSur VALUES ('Minecraft',4);
COMMIT;


BEGIN TRANSACTION;
INSERT INTO RessourceDisponibleSur VALUES ('Personnage1',1);
INSERT INTO RessourceDisponibleSur VALUES ('Personnage1',3);
INSERT INTO RessourceDisponibleSur VALUES ('Personnage1',4);
INSERT INTO RessourceDisponibleSur VALUES ('PersonnageDeMinecraft',1);
INSERT INTO RessourceDisponibleSur VALUES ('PersonnageDeMinecraft',3);
INSERT INTO RessourceDisponibleSur VALUES ('PersonnageDeMinecraft',4);
INSERT INTO RessourceDisponibleSur VALUES ('Multiplication',1);
INSERT INTO RessourceDisponibleSur VALUES ('Multiplication',2);
INSERT INTO RessourceDisponibleSur VALUES ('Multiplication',3);
INSERT INTO RessourceDisponibleSur VALUES ('Multiplication',4);
ROLLBACK;
BEGIN TRANSACTION;
INSERT INTO RessourceDisponibleSur VALUES ('Personnage1',1);
INSERT INTO RessourceDisponibleSur VALUES ('Personnage1',3);
INSERT INTO RessourceDisponibleSur VALUES ('Personnage1',4);
INSERT INTO RessourceDisponibleSur VALUES ('PersonnageDeMinecraft',1);
INSERT INTO RessourceDisponibleSur VALUES ('PersonnageDeMinecraft',3);
INSERT INTO RessourceDisponibleSur VALUES ('PersonnageDeMinecraft',4);
INSERT INTO RessourceDisponibleSur VALUES ('Multiplication',1);
INSERT INTO RessourceDisponibleSur VALUES ('Multiplication',2);
INSERT INTO RessourceDisponibleSur VALUES ('Multiplication',3);
INSERT INTO RessourceDisponibleSur VALUES ('Multiplication',4);
COMMIT;

